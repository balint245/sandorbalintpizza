﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestPizza
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FillCustomerComboBox();
            FillProductComboBox();
            FillCouriersComboBox();
        }

        private void FillCustomerComboBox()
        {
            using(testPizzaEntities aEntities = new testPizzaEntities())
            {
                nameComboBox.DataSource = aEntities.Customers.ToList();
                nameComboBox.ValueMember = "Id";
                nameComboBox.DisplayMember = "Name";
            }
            nameComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            nameComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            nameComboBox.DropDownStyle = ComboBoxStyle.DropDown;
        }

        private void FillProductComboBox()
        {
            using(testPizzaEntities aEntities = new testPizzaEntities())
            {
                foodComboBox.DataSource = aEntities.Products.ToList();
                foodComboBox.ValueMember = "Id";
                foodComboBox.DisplayMember = "FoodType";
            }
            foodComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            foodComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            foodComboBox.DropDownStyle = ComboBoxStyle.DropDown;
        }

        private void FillAddressComboBox(int id)
        {
            using (testPizzaEntities aEntities = new testPizzaEntities())
            {
                var addresses = from Address in aEntities.Addresses
                                where Address.CustomerId == id
                                select Address.Address1;

                addressComboBox.DataSource = addresses.ToList();
                //addressComboBox.ValueMember = "Id";
                addressComboBox.DisplayMember = "Address1";
            }
            addressComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            addressComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            addressComboBox.DropDownStyle = ComboBoxStyle.DropDown;
        }

        public void FillCouriersComboBox()
        {
            using(testPizzaEntities aEntities = new testPizzaEntities())
            {
                var availableCouriers = from Courier in aEntities.Couriers
                                        where Courier.Available == 1
                                        select Courier.CourierName;

                couriersComboBox.DataSource = availableCouriers.ToList();
                couriersComboBox.DisplayMember = "CourierName";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Customer newCustomer = new Customer();
                newCustomer.Name = nameTextBox.Text;
                newCustomer.Address = addressTextBox.Text;
                saveToDatabase(newCustomer);
            }
            catch(Exception msg)
            {
                MessageBox.Show(msg.ToString());
            }
        }

        private void saveToDatabase(Customer newCustomer)
        {
            using (testPizzaEntities aEntities = new testPizzaEntities())
            {
                try
                {
                    aEntities.Customers.Add(newCustomer);
                    aEntities.SaveChanges();
                }catch(Exception msg)
                {
                    MessageBox.Show(msg.ToString());
                }
                
            }

            using (testPizzaEntities aEntities = new testPizzaEntities())
            {
                try
                {
                    Address newAddress = new Address();
                    int id = newCustomer.Id;
                    newAddress.CustomerId = id;
                    newAddress.Address1 = addressTextBox.Text;
                    aEntities.Addresses.Add(newAddress);
                    aEntities.SaveChanges();
                }
                catch(Exception msg)
                {
                    MessageBox.Show(msg.ToString());
                }
            }

            FillCustomerComboBox();

            MessageBox.Show(nameTextBox.Text+" has been added to database, with "+addressTextBox.Text+" address.");

            nameTextBox.Text = "";
            addressTextBox.Text = "";
        }

        private void nameComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            int id = nameComboBox.SelectedIndex + 1;
            FillAddressComboBox(id);

            var context = new testPizzaEntities();
            string query = (from Customer in context.Customers
                            where Customer.Id == id
                            select Customer.Address).SingleOrDefault();
            addressLabel.Text = query;
        }

        private void foodComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = foodComboBox.SelectedIndex + 1;

            var context = new testPizzaEntities();
            var query = context.Products.FirstOrDefault(c => c.Id == id);
            priceLabel.Text = (query.Price.ToString() + " Ft");
        }

        private void quantityTextBox_TextChanged(object sender, EventArgs e)
        {
            int id = foodComboBox.SelectedIndex + 1;
            var context = new testPizzaEntities();
            var query = context.Products.FirstOrDefault(c => c.Id == id);
            if (System.Text.RegularExpressions.Regex.IsMatch(quantityTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                quantityTextBox.Text = quantityTextBox.Text.Remove(quantityTextBox.Text.Length - 1);
            }
            else if(quantityTextBox.Text == "")
            {
                priceLabel.Text = (query.Price.ToString() + " Ft");
            }
            else
            {
                priceLabel.Text = ((query.Price * Int32.Parse(quantityTextBox.Text)).ToString() + " Ft");
            }
        }

        private void addAddressButton_Click(object sender, EventArgs e)
        {
            int id = nameComboBox.SelectedIndex + 1;

            //hozzáadunk új címet a jelenleg kiválasztott vendéghez
            using(testPizzaEntities aEntities = new testPizzaEntities())
            {
                aEntities.Addresses.Add(new Address() { Address1 = newAddressTextBox.Text, CustomerId = id });
                aEntities.SaveChanges();
            }
            FillAddressComboBox(id);
        }

        
        private void startOrderButton_Click(object sender, EventArgs e)
        {
            string currentName = this.nameComboBox.GetItemText(this.nameComboBox.SelectedItem);
            string currentAddress = this.addressComboBox.GetItemText(this.addressComboBox.SelectedItem);
            string currentProduct = this.foodComboBox.GetItemText(this.foodComboBox.SelectedItem);
            int currentPrice = Int32.Parse(this.priceLabel.Text.Substring(0, this.priceLabel.Text.Length-3)); //"Ft"-t kitöröljük


            int id = couriersComboBox.SelectedIndex + 1;
            string courirerName = this.couriersComboBox.GetItemText(this.couriersComboBox.SelectedItem);
            using (testPizzaEntities aEntities = new testPizzaEntities())
            {
                var selectedCourier = aEntities.Couriers.SingleOrDefault(c => c.CourierName == courirerName);
                selectedCourier.Available = 0;
                selectedCourier.CustomerAddress = currentAddress;
                selectedCourier.CustomerName = currentName;
                selectedCourier.ProductName = currentProduct;
                selectedCourier.Price = currentPrice;

                aEntities.SaveChanges();

                FillCouriersComboBox();

                //átadjuk az információkat a következő az "OrderedForm"-nak
                OrderedForm oF = new OrderedForm(selectedCourier);
                oF.Show();
            }


            
            
        }
    }
}
