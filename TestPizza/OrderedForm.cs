﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestPizza
{
    public partial class OrderedForm : Form
    {
        enum Status { Prepare, Posted, Received };
        private Status currentStatus;
        public Courier currCourier;

        public OrderedForm(Courier currentCourier)
        {
            InitializeComponent();

            currCourier = currentCourier;

            nameLabel.Text = currCourier.CustomerName;
            addressLabel.Text = currCourier.CustomerAddress;
            productLabel.Text = currCourier.ProductName;
            priceLabel.Text = currCourier.Price.ToString();

            currentStatus = Status.Prepare;
            statusLabel.Text = currentStatus.ToString();
        }

        private void nextStatusButton_Click(object sender, EventArgs e)
        {
            if(statusLabel.Text == Status.Received.ToString())
            {
                using(testPizzaEntities aEntities = new testPizzaEntities())
                {
                    var updateCuorier = aEntities.Couriers.SingleOrDefault(c => c.Id == currCourier.Id);
                    updateCuorier.Available = 1;
                    updateCuorier.CustomerName = null;
                    updateCuorier.CustomerAddress = null;
                    updateCuorier.ProductName = null;
                    updateCuorier.Price = null;

                    aEntities.SaveChanges();
                }
                Form1 form1 = new Form1();
                form1.FillCouriersComboBox();

                MessageBox.Show("Customer took the ordered food.");
                this.Close();
            }
            else
            {
                currentStatus++;
                statusLabel.Text = currentStatus.ToString();
            }
        }
    }
}
